const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Link = require('../models/Link');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
  // Product index
  router.get('/', (req, res) => {
    Link.find()
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });


  router.post('/', (req, res) => {
    const linkData = req.body;
    console.log(req.body.originalUrl)
    linkData.shortUrl = nanoid(7);

    const link = new Link(linkData);

    link.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  // link get by ID
  router.get('/:id', (req, res) => {
    Link.findOne({shortUrl: req.params.id}, (error, result) => {
      if (result) res.status(301).redirect(result.originalUrl);
      else res.sendStatus(404);
    })

  });

  return router;
};

module.exports = createRouter;